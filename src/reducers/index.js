
const initialState = {
  timerCount: 0,
  count: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "START":
            return {
                timerCount: state.timerCount,
                count: action.count
            }
        case "STOP": 
            return state
        case "COUNT":
            return { 
                ...state, 
                timerCount: state.timerCount + 1 
            }
        case "RESET":
            return {
                ...state, 
                timerCount: 0
            }
        default:
        return state;
    }
};

export default reducer;