export const start = () => {
    return {
        type: 'START'
    }
}

export const stop = () => {
    return {
        type: 'STOP'
    }
}

export const reset = () => {
    return {
        type: 'RESET'
    }
}

export const count = () => {
    return {
        type: 'COUNT'
    }
}