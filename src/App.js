import React from 'react';
// import './App.css';
import { connect } from "react-redux" 
import { stop, reset } from "./actions"



class App extends React.Component {
  handleStop = () => {
    clearInterval(this.props.count);
    this.props.stopTimer();
  };

  render() {
    console.log(this.props)
    let centiseconds = ("0" + (this.props.timerCount % 60)).slice(-2);
    let seconds = ("0" + (Math.floor(this.props.timerCount / 60) % 60)).slice(-2);
    let minutes = ("0" + (Math.floor(this.props.timerCount / 3600) % 60)).slice(-2);
    let hours = ("0" + Math.floor(this.props.timerCount / 216000) % 60).slice(-2);
    return (
      <div>
        {hours} : {minutes} : {seconds} : {centiseconds}
        <br />
          <button onClick={this.props.startTimer}>Start</button>
          <button onClick={this.handleStop}>Stop</button>
          <button onClick={this.props.startTimer}>Resume</button>
          <button onClick={this.props.resetTimer}>Reset</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    timerCount: state.timerCount,
    count: state.count
  };
};

const mapDispatchToProps = dispatch => {
  return {
    startTimer: () => {
      const count = setInterval(() => dispatch({type: 'COUNT'}), 1000/60);
      dispatch({ type: "START", count });
    },  
    stopTimer: () => {
      dispatch(stop());
    },
    resetTimer: () => dispatch(reset())
  };
};

const Connection = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default Connection